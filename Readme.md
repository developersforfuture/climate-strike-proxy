#Time window based reverse proxy

This proxy will, given a default target uri, and two time infos, proxy all traffic to the targetUri unless the current timestamp is between the given ones.
In this case it would display a static html file with infos about the request.

It originates from the https://shutdownforclimate.de/ (2019-09-20) but can be used for maintenance windows as well.

### How to build

Run `docker build -t climate-strike-proxy .`


### Configuration
Use the following environment variables:

Variable | Meaning | Default
--- | --- | ---
PORT | The port this reverse proxy will run on | (none)
DEFAULT_TARGET | Upstream-URL if not active | (none)
ACTIVATION | When to  enable the proxy | 2019-09-19-21-00-00
DEACTIVATION | When to diable again | 2019-09-21-03-00-00

### Template
You can mount a file `/static/index.html` into this proxy. It will be parsed using template (https://golang.org/pkg/text/template/), receiving the http.Request object as interface, so all informations from the request can be used in the template.
A default Template for climate strike is included.

The builtin template is the one from http://shutdownforclimate.de/wp-content/plugins/shutdownForClimate/assets/down.html 
where the host (including port) will be insertet as webaddress. The path will be ignored by default.