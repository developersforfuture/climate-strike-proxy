package main

import (
	"html/template"
	"log"
	"os"
	"path/filepath"
	"time"

	"net/http"
	"net/http/httputil"
	"net/url"
)

var proxy *httputil.ReverseProxy
var tpl = template.Must(template.ParseFiles("/static/index.html"))

var from time.Time
var until time.Time

const LAYOUT = "2006-01-02-15-04-05"

func main() {
	printWorkdir()

	defaultFrom, _ := time.Parse(LAYOUT, "2019-09-19-21-00-00")
	defaultUntil, _ := time.Parse(LAYOUT, "2019-09-21-03-00-00")
	from = getTimeFromEnvironment("ACTIVATION", defaultFrom)
	until = getTimeFromEnvironment("DEACTIVATION", defaultUntil)

	targetUrl, err2 := url.Parse(os.Getenv("DEFAULT_TARGET"))
	if err2 != nil {
		log.Panic(err2)
	}
	proxy = httputil.NewSingleHostReverseProxy(targetUrl)

	if proxy == nil {
		log.Panic("DEFAULT_TARGET is not a valid url.")
	}
	http.HandleFunc("/", serverIngress)
	err := http.ListenAndServe(":"+os.Getenv("PORT"), nil)
	if err != nil {
		log.Panic(err)
	}
}

func getTimeFromEnvironment(envVar string, defaultTime time.Time) time.Time {

	res, err := getTimeFromEnvironmentVariable(envVar)
	if err != nil {
		res = defaultTime
		log.Println("Could not parse " + envVar + ", using climate strike default: " + defaultTime.Format(LAYOUT))
		log.Println(err)
	}
	return res
}

func serverIngress(rw http.ResponseWriter, req *http.Request) {
	if inTimeWindow() {
		err := tpl.Execute(rw, req)
		if err != nil {
			log.Panic(err)
		}
	} else {
		proxy.ServeHTTP(rw, req)
	}
}

func printWorkdir() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	log.Println(dir)
}

func getTimeFromEnvironmentVariable(envName string) (time.Time, error) {
	return time.Parse(LAYOUT, os.Getenv(envName))
}

func inTimeWindow() bool {
	now := time.Now()
	return from.Before(now) && until.After(now)
}
